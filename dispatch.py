import extract
from table2ascii import table2ascii as t2a, PresetStyle, Alignment


def get_response(message: str) -> str:
    p_message = message.lower()

    if p_message == '/sneak':
        movies = extract.scrape_sneak()

        # ugly table
        output = t2a(
            header=movies[0].keys(),
            body=[movie.values() for movie in movies],
            style=PresetStyle.thin_compact,
            alignments=[Alignment.CENTER, Alignment.LEFT, Alignment.LEFT, Alignment.CENTER, Alignment.CENTER]
        )

        return f"```\n{output}\n```"
