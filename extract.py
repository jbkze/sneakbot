import requests
from bs4 import BeautifulSoup
import imdb


def scrape_sneak():
    # URL der Seite
    url = "http://5.45.100.152/sneakprognose.php?SPO=Leipzig&SPK=Cinestar&SPN=Sneak+Preview"

    # Quelltext herunterladen
    response = requests.get(url)
    html_content = response.text

    # BeautifulSoup verwenden, um den Quelltext zu analysieren und die Informationen zu extrahieren
    soup = BeautifulSoup(html_content, "html.parser")

    # Liste zur Speicherung der extrahierten Informationen
    movies = []

    # Durch die Tabellenzeilen iterieren und Informationen extrahieren
    for row in soup.find_all("tr", class_="lvct2")[:5]:
        columns = row.find_all("td")

        title = columns[3].a.text.strip()
        genre_and_year = columns[3].text.split(title)[1]
        genre_and_year = genre_and_year.split("von")[0].strip()
        year, genre = genre_and_year[:4], genre_and_year[5:]

        genre = ", ".join(genre.split(' / '))

        imdb_link = imdb.get_imdb_link(title, year)
        rating = imdb.get_rating(imdb_link)
        #image_link = imdb.get_image(imdb_link)

        movie = {
            "Rank": columns[0].text.strip(),
            "Title": title,
            "Genre": genre,
            "Year": year,
            #"IMDB": imdb_link,
            #"Cover": image_link,
            "IMDB": rating
        }
        movies.append(movie)

    return movies


# movies = scrape_sneak()
#
# output = t2a(
#     header=movies[0].keys(),
#     body=[movie.values() for movie in movies],
#     style=PresetStyle.thin_compact
# )
#
# print(output)

# # Informationen extrahieren
# movies = scrape_sneak()
#
# # Informationen ausgeben
# for movie in movies:
#     print("Rank:", movie["Rank"])
#     print("Title:", movie["Title"])
#     print("Genre:", movie["Genre"])
#     print("Year:", movie["Year"])
#     print("IMDB:", movie["IMDB"])
#     print("Cover:", movie["Cover"])
#     print("Rating:", movie["Rating"])
#     print("---")